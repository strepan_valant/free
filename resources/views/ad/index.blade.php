@extends('layouts.main')

@section('title', $ad->title)

@section('content')
    @include('main.partials.header', ['title' => $ad->title])

    <div class="container">
        <div class="buy-single-single">
            <div class="col-md-9 single-box">
                <div class=" buying-top">
                    <div class="flexslider">
                        <div class="flex-viewport" style="overflow: hidden; position: relative;">
                            <ul class="slides"
                                style="width: 1200%; transition-duration: 0s; transform: translate3d(-845px, 0px, 0px);">
                                <li data-thumb="{{ $ad->images[count($ad->images)-1]->link }}" class="clone"
                                    aria-hidden="true" style="width: 845px; float: left; display: block;">
                                    <img src="{{ $ad->images[count($ad->images)-1]->link }}" draggable="false">
                                </li>
                                @foreach($ad->images as $key => $image)
                                    <li class="{{ ($key === 0) ? 'flex-active-slide' : '' }}"
                                        data-thumb="{{ $image->link }}"
                                        style="width: 845px; float: left; display: block;" class="">
                                        <img src="{{ $image->link }}" draggable="false">
                                    </li>
                                @endforeach
                                <li data-thumb="{{ $ad->images[0]->link }}" class="clone" aria-hidden="true"
                                    style="width: 845px; float: left; display: block;">
                                    <img src="{{ $ad->images[0]->link }}" draggable="false">
                                </li>
                            </ul>
                        </div>
                        <ol class="flex-control-nav flex-control-thumbs">
                            @foreach($ad->images as $key => $image)
                                <li>
                                    <img class="{{ ($key === 0) ? 'flex-active' : '' }}" src="{{ $image->link }}"
                                         draggable="false" class="">
                                </li>
                            @endforeach
                        </ol>
                        <ul class="flex-direction-nav">
                            <li class="flex-nav-prev"><a class="flex-prev" href="#">Previous</a></li>
                            <li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li>
                        </ul>
                    </div>
                </div>
                <div class="buy-sin-single">
                    <div class="col-sm-5 middle-side immediate">
                        <table>
                            <tr>
                                <td><b>Состояние</b></td>
                                <td>: <span class="two">{{ $ad->getCondition() }}</span></td>
                            </tr>
                            <tr>
                                <td><b>На обмен</b></td>
                                <td>: <span class="two">{{ getYesOrNo($ad->is_to_exchange) }}</span></td>
                            </tr>
                            <tr>
                                <td><b>Самовывоз</b></td>
                                <td>: <span class="two">{{ getYesOrNo($ad->is_to_pickup) }}</span></td>
                            </tr>
                        </table>
                        <div class="   right-side">
                            <a href="contact.html" class="hvr-sweep-to-right more">Contact Builder</a>
                        </div>
                    </div>
                    <div class="col-sm-7 buy-sin">
                        <h4>Описание</h4>
                        <p>
                            {{ $ad->description }}
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="map-buy-single">
                    <h4>Местоположение</h4>
                    <div class="map-buy-single1">
                        <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA6pZnr40sYbo1_tEX_aRH-NxL9iWACLi0&q={{ "{$ad->city->name} {$ad->pickup_address}" }}"
                                allowfullscreen></iframe>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="single-box-right right-immediate">
                    <h4>Featured Communities</h4>
                    <div class="single-box-img ">
                        <div class="box-img">
                            <a href="single.html"><img class="img-responsive" src="images/sl.jpg" alt=""></a>
                        </div>
                        <div class="box-text">
                            <p><a href="single.html">Lorem ipsum dolor sit amet</a></p>
                            <a href="single.html" class="in-box">More Info</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="single-box-img">
                        <div class="box-img">
                            <a href="single.html"><img class="img-responsive" src="images/sl1.jpg" alt=""></a>
                        </div>
                        <div class="box-text">
                            <p><a href="single.html">Lorem ipsum dolor sit amet</a></p>
                            <a href="single.html" class="in-box">More Info</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="single-box-img">
                        <div class="box-img">
                            <a href="single.html"><img class="img-responsive" src="images/sl2.jpg" alt=""></a>
                        </div>
                        <div class="box-text">
                            <p><a href="single.html">Lorem ipsum dolor sit amet</a></p>
                            <a href="single.html" class="in-box">More Info</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="single-box-img">
                        <div class="box-img">
                            <a href="single.html"><img class="img-responsive" src="images/sl3.jpg" alt=""></a>
                        </div>
                        <div class="box-text">
                            <p><a href="single.html">Lorem ipsum dolor sit amet</a></p>
                            <a href="single.html" class="in-box">More Info</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="single-box-img">
                        <div class="box-img">
                            <a href="single.html"><img class="img-responsive" src="images/sl4.jpg" alt=""></a>
                        </div>
                        <div class="box-text">
                            <p><a href="single.html">Lorem ipsum dolor sit amet</a></p>
                            <a href="single.html" class="in-box">More Info</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>

@stop