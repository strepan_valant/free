<html>
<head>
    <title>FREE - @yield('title')</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! HTML::style('css/bootstrap.css') !!}
    {!! HTML::script('js/jquery.min.js') !!}
    {!! HTML::script('js/jquery.ui.widget.js') !!}

    {!! HTML::script('js/fileupload/jquery.fileupload.js') !!}
    {!! HTML::script('js/fileupload/jquery.fileupload-process.js') !!}
    {!! HTML::script('js/fileupload/jquery.fileupload-image.js') !!}
    {!! HTML::script('js/fileupload/jquery.fileupload-validate.js') !!}
    {!! HTML::script('js/fileupload/jquery.fileupload-ui.js') !!}

    {!! HTML::script('js/select2.full.js') !!}
    {!! HTML::style('css/select2.min.css') !!}

    {!! HTML::script('js/jquery.dataTables.min.js') !!}
    {!! HTML::style('css/jquery.dataTables.min.css') !!}

    {!! HTML::script('js/responsiveslides.min.js') !!}

    {!! HTML::script('js/bootstrap/modal.js') !!}

    {!! HTML::script('js/fancybox/jquery.fancybox.js') !!}
    {!! HTML::style('css/fancybox/jquery.fancybox.css') !!}

    {!! HTML::script('js/flexslider/jquery.flexslider.js') !!}
    {!! HTML::style('css/flexslider/flexslider.css') !!}

    {!! HTML::style('css/styles.css') !!}
    {!! HTML::style('css/style.css') !!}
    {!! HTML::script('js/scripts.js') !!}
</head>
<body>
<div class="header">
    <div class="container">
        <div class="logo">
            <h1><a href="/">FREE</a></h1>
        </div>
        <div class="top-nav">
            <ul class="right-icons">
                @if($user = Auth::user())
                    <li>
                        <a href="/profile">
                            @if($user->has_avatar)
                                <img src="{{ $user->getAvatar() }}">
                            @else
                                <i class="glyphicon glyphicon-user"> </i>
                            @endif
                            {{ $user->login }}
                        </a>
                    </li>
                    <li><a href="/logout">Выйти</a></li>
                @else
                    <li><a href="/login"><i class="glyphicon glyphicon-user"> </i>Войти</a></li>
                @endif
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

@yield('content')

<div class="footer">
    <div class="container">
        <div class="footer-top-at">
            <div class="col-md-4 amet-sed">
                <ul class="nav-bottom">
                    <li><a href="/about">О проекте</a></li>
                    <li><a href="/terms">Условия использования</a></li>
                    <li><a href="/privacy">Политика конфиденциальности</a></li>
                    <li><a href="/popular">Популярные запросы</a></li>
                </ul>
            </div>
            <div class="col-md-4 amet-sed ">
                <ul class="nav-bottom">
                    <li><a href="/help">Помощь</a></li>
                    <li><a href="/faq">Ответы и вопросы</a></li>
                    <li><a href="/sitemap">Карта сайта</a></li>
                    <li><a href="/archive">Архив</a></li>
                </ul>
            </div>
            <div class="col-md-4 amet-sed ">
                <ul class="nav-bottom">
                    <li><a href="/login">Войти</a></li>
                    <li><a href="/register">Регистрация</a></li>
                    <li><a href="/feedback">Обратная связь</a></li>
                </ul>
                <ul class="social">
                    <li><a href="#"><i> </i></a></li>
                    <li><a href="#"><i class="gmail"> </i></a></li>
                    <li><a href="#"><i class="twitter"> </i></a></li>
                    <li><a href="#"><i class="camera"> </i></a></li>
                    <li><a href="#"><i class="dribble"> </i></a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="col-md-4 footer-logo">
                <h2><a href="/">FREE</a></h2>
            </div>
            <div class="col-md-8 footer-class">
                <p>© {{ date("Y") }} Yevhen Strepan | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a>
                </p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</body>
</html>