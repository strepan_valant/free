@extends('layouts.main')

@section('title', 'Мой профиль')

@section('content')
    @include('main.partials.header', ['title' => 'Мой профиль'])

    <div class="profile">
    	<div class="container">
			@include('profile.partials.profile-menu', ['page' => 'profile'])
    		<div class="loan-col">
    		    <h3>Профиль</h3>
    		    <div class="alerts">
    		    @if(Session::has('error-user') && $errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @elseif(Session::has('success-user'))
                    <div class="alert alert-success">
                        {{ Session::get('success-user') }}
                    </div>
                @endif
    		    </div>
                {!! Form::model($user, array('url' => 'profile', 'files' => true)) !!}
                <div id="profile-left">
                    <img src="{{ $user->getAvatar() }}"><br>
                    <button id="change-avatar" type="button" class="btn btn-sm btn-primary">{{ $user->has_avatar ? 'Изменить' : 'Добавить' }}</button>
                    <button id="remove-avatar" type="button" class="btn btn-sm btn-default" {{ $user->has_avatar ? '' : 'style="display: none"' }}>Удалить</button>
                    <input class="fileupload" name="avatar" type="file" style="display: none">
                    {!! Form::hidden('has_avatar', $user->has_avatar, ['name' => 'User[has_avatar]', 'id' => 'has_avatar']) !!}
                </div>
    			<div id="profile-right">
                    <div class="col-loan">
                        <ul class="loan-col1">
                            <li>
                                {!! Form::label('email', 'E-mail') !!}
                            </li>
                            <label>:</label>
                            <li class="loan-list-top">
                                {!! Form::email('email', $user->email, ['name' => 'User[email]']) !!}
                            </li>
                        </ul>
                        <ul class="loan-col1">
                            <li>
                                {!! Form::label('login', 'Логин') !!}
                            </li>
                            <label>:</label>
                            <li class="loan-list-top">
                                {!! Form::text('login', $user->login, ['name' => 'User[login]']) !!}
                            </li>
                        </ul>
                        <ul class="loan-col1">
                            <li>
                                {!! Form::label('password', 'Пароль (если новый)') !!}
                            </li>
                            <label>:</label>
                            <li class="loan-list-top">
                                {!! Form::password('password', ['name' => 'User[password]']) !!}
                            </li>
                        </ul>
                        <ul class="loan-col1">
                            <li>
                                {!! Form::label('first_name', 'Имя') !!}
                            </li>
                            <label>:</label>
                            <li class="loan-list-top">
                                {!! Form::text('first_name', $user->first_name, ['name' => 'User[first_name]']) !!}
                            </li>
                        </ul>
                        <ul class="loan-col1">
                            <li>
                                {!! Form::label('last_name', 'Фамилия') !!}
                            </li>
                            <label>:</label>
                            <li class="loan-list-top">
                                {!! Form::text('last_name', $user->last_name, ['name' => 'User[last_name]']) !!}
                            </li>
                        </ul>
                        <ul class="loan-col1">
                            <li>
                                {!! Form::label('birthday', 'День рождения') !!}
                            </li>
                            <label>:</label>
                            <li class="loan-list-top">
                                <input type="date" placeholder="Дата рождения" name="User[birthday]" value="{{ $user->birthday }}" required="">
                            </li>
                        </ul>

                        <div class="sub">
                            <label class="hvr-sweep-to-right">
                            {!! Form::submit('Сохранить') !!}
                            </label>
                        </div>
                    </div>
    			</div>
    			{!! Form::close() !!}
    			<div style="clear: both;"></div>
    		</div>
    	</div>
    </div>
@stop