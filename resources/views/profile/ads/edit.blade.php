@extends('layouts.main')

@section('title', 'Мой профиль')

@section('content')
    @include('main.partials.header', ['title' => 'Мой профиль'])

    <div class="profile">
        <div class="container">
            @include('profile.partials.profile-menu', ['page' => 'ads'])
            <div class="loan-col">
                <h3>Редактирование</h3>
                @if(Session::has('error-profile') && $errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @elseif(Session::has('success-profile'))
                    <div class="alert alert-success">
                        {{ Session::get('success-profile') }}
                    </div>
                @endif
                {!! Form::model($ad, array('url' => 'profile/ads/' . $ad->id . '/edit')) !!}
                <table class="add-ad">
                    <tr>
                        <td>
                            {!! Form::label('title', 'Заголовок') !!} <span>*</span>
                        </td>
                        <td>
                            {!! Form::text('title', $ad->title, ['name' => 'Ad[title]']) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('description', 'Описание') !!} <span>*</span>
                        </td>
                        <td>
                            {!! Form::textarea('description', $ad->description, ['name' => 'Ad[description]']) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('category_id', 'Категория') !!} <span>*</span>
                        </td>
                        <td>
                            {!! Form::select('category_id', ['' => 'Выберите категорию'] + App\Category::getCategories()->lists('name', 'id'), null, [
                                'name' => 'Ad[category_id]',
                                'class' => 'categories-select'
                            ]) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('condition', 'Состояние') !!} <span>*</span>
                        </td>
                        <td>
                            {!! Form::radio('condition', true, $ad->condition, ['name' => 'Ad[condition]']) !!} Новое<br>
                            {!! Form::radio('condition', false, !$ad->condition, ['name' => 'Ad[condition]']) !!} Б/у
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('condition', 'Тип объявления') !!} <span>*</span>
                        </td>
                        <td>
                            {!! Form::radio('is_gift', true, $ad->is_gift, ['name' => 'Ad[is_gift]']) !!} Отдам<br>
                            {!! Form::radio('is_no_gift', false, !$ad->is_gift, ['name' => 'Ad[is_gift]']) !!} Приму
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('condition', 'На обмен') !!}
                        </td>
                        <td>
                            {!! Form::checkbox('is_to_exchange', true, $ad->is_to_exchange, ['name' => 'Ad[is_to_exchange]']) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('city_id', 'Город') !!} <span>*</span>
                        </td>
                        <td>
                            {!! Form::text('city_id', $ad->city_id, [
                                'data-id' => $ad->city_id,
                                'data-name' => $ad->city->name,
                                'data-region' => $ad->city->region->name,
                                'name' => 'Ad[city_id]',
                                'class' => 'cities-ajax-select'
                            ]) !!}
                        </td>
                    </tr>
                    <tr class="sub">
                        <td>
                            <label class="hvr-sweep-to-right">
                                <input type="submit" value="Разместить">
                            </label>
                        </td>
                    </tr>
                </table>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop