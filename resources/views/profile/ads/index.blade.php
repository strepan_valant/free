@extends('layouts.main')

@section('title', 'Мой профиль')

@section('content')
    @include('main.partials.header', ['title' => 'Мой профиль'])

    <div class="profile">
    	<div class="container">
			@include('profile.partials.profile-menu', ['page' => 'ads'])
            <div class="loan-col">
                <h3>Мои объявления</h3>
                <button type="button" class="btn btn-lg btn-warning" onclick="location.href='/profile/ads/add'">Подать объявление</button>
                @if(Session::has('error-profile') && $errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @elseif(Session::has('success-profile'))
                    <div class="alert alert-success">
                        {{ Session::get('success-profile') }}
                    </div>
                @endif
                <table id="user-ads" class="display" cellspacing="0" width="100%"></table>
            </div>
    	</div>
    </div>
@stop