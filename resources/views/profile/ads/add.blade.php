@extends('layouts.main')

@section('title', 'Мой профиль')

@section('content')
    @include('main.partials.header', ['title' => 'Мой профиль'])

    <div class="profile">
        <div class="container">
            @include('profile.partials.profile-menu', ['page' => 'ads'])
            <div class="loan-col">
                <h3>Подача объявления</h3>
                @if(Session::has('error-profile') && $errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @elseif(Session::has('success-profile'))
                    <div class="alert alert-success">
                        {{ Session::get('success-profile') }}
                    </div>
                @endif
                {!! Form::model($ad, array('url' => 'profile/ads/add', 'files' => true)) !!}
                <table class="add-ad">
                    <tr>
                        <td>
                            {!! Form::label('title', 'Заголовок') !!} <span>*</span>
                        </td>
                        <td>
                            {!! Form::text('title', $ad->title, ['name' => 'Ad[title]']) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('description', 'Описание') !!} <span>*</span>
                        </td>
                        <td>
                            {!! Form::textarea('description', $ad->description, ['name' => 'Ad[description]']) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('category_id', 'Категория') !!} <span>*</span>
                        </td>
                        <td>
                            {!! Form::select('category_id', ['' => 'Выберите категорию'] + App\Category::getCategories()->lists('name', 'id'), null, [
                                'name' => 'Ad[category_id]',
                                'class' => 'categories-select'
                            ]) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('condition', 'Состояние') !!} <span>*</span>
                        </td>
                        <td>
                            {!! Form::radio('condition', $ad->condition, true, ['name' => 'Ad[condition]']) !!}
                            Новое<br>
                            {!! Form::radio('condition', $ad->condition, false, ['name' => 'Ad[condition]']) !!} Б/у
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('condition', 'Тип объявления') !!} <span>*</span>
                        </td>
                        <td>
                            {!! Form::radio('is_gift', true, $ad->is_gift, ['name' => 'Ad[is_gift]']) !!} Отдам<br>
                            {!! Form::radio('is_no_gift', false, $ad->is_no_gift, ['name' => 'Ad[is_gift]']) !!} Приму
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('condition', 'На обмен') !!}
                        </td>
                        <td>
                            {!! Form::checkbox('is_to_exchange', true, $ad->is_to_exchange, ['name' => 'Ad[is_to_exchange]']) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('city_id', 'Местоположение') !!} <span>*</span>
                        </td>
                        <td>
                            {!! Form::text('city_id', $profile->city_id, [
                                'data-id' => $profile->city_id,
                                'data-name' => $profile->city->name,
                                'data-region' => $profile->city->region->name,
                                'name' => 'Ad[city_id]',
                                'class' => 'cities-ajax-select'
                            ]) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('is_to_pickup', 'Самовывоз') !!}
                        </td>
                        <td>
                            {!! Form::checkbox('is_to_pickup', true, $ad->is_to_pickup, ['name' => 'Ad[is_to_pickup]', 'id' => 'is_to_pickup']) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::label('is_to_pickup', 'Адрес') !!}<br>
                            (самовывоза)
                        </td>
                        <td>
                            {!! Form::textarea('pickup_address', $ad->pickup_address, ['name' => 'Ad[pickup_address]', 'id' => 'pickup_address']) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="city_id">Фотографии</label><br>
                            (максимум 6 штук)
                        </td>
                        <td>
                            <div class="photos">
                                {{--@for($i = 0; $i < 6; $i++)
                                <div class="photo">
                                    <a class="fancybox" href="#" rel="photo"><img src="/images/bo.jpg"></a>
                                    <input type="hidden" name="Images[{{ $image->id }}]" value="{{ $image->link }}">
                                </div>
                                @endfor--}}
                                @for($i = 0; $i < 6; $i++)
                                    <div class="photo empty">
                                        <div class="trash">
                                            <i></i>
                                        </div>
                                        <a class="fancybox" href=""><img src=""></a>
                                        <input class="fileupload" type="file" name="image" value="">
                                        <input class="filename" type="hidden" name="Images[new][]" value="">
                                    </div>
                                @endfor
                                <div class="clr"></div>
                            </div>
                            <div>
                                <button id="add-photo" type="button" class="btn btn-sm btn-primary"
                                        style="margin: 0 auto; display: block;">Добавить фото
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr class="sub">
                        <td>
                            <label class="hvr-sweep-to-right">
                                <input type="submit" value="Разместить">
                            </label>
                        </td>
                    </tr>
                </table>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            $('table.add-ad .photos .photo .trash').on('click', function () {
                $('#myModal').modal('show');
            });

        });
    </script>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p>Вы уверены, что хотите удалить фотографию?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
                    <button type="button" class="btn btn-primary">Да</button>
                </div>
            </div>
        </div>
    </div>
@stop