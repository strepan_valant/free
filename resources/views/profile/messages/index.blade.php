@extends('layouts.main')

@section('title', 'Мой профиль')

@section('content')
    @include('main.partials.header', ['title' => 'Мой профиль'])

    <div class="profile">
    	<div class="container">
			@include('profile.partials.profile-menu', ['page' => 'messages'])

    	</div>
    </div>
@stop