@extends('layouts.main')

@section('title', 'Мой профиль')

@section('content')
    @include('main.partials.header', ['title' => 'Мой профиль'])

    <div class="profile">
    	<div class="container">
			@include('profile.partials.profile-menu', ['page' => 'messages'])
            <div class="dialog">
                <div class="message left">
                    <div class="avatar">
                        <img src="/images/ga2.jpg">
                        <p>16.12.2015</p>
                        <p>16:30</p>
                    </div>
                    <div class="text">
                        <div class="author">Ванесса Мэй</div>
                        <p>asdasd</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="message right">
                    <div class="avatar">
                        <img src="/images/ga2.jpg">
                        <p>16.12.2015</p>
                        <p>16:30</p>
                    </div>
                    <div class="text">
                        <div class="author">Ваше сообщение</div>
                        <p>asdasd</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="message left">
                    <div class="avatar">
                        <img src="/images/ga2.jpg">
                        <p>Вчера</p>
                        <p>16:30</p>
                    </div>
                    <div class="text">
                        <div class="author">Ванесса Мэй</div>
                        <p>asdasd</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="message right">
                    <div class="avatar">
                        <img src="/images/ga2.jpg">
                        <p>Сегодня</p>
                        <p>16:30</p>
                    </div>
                    <div class="text">
                        <div class="author">Ваше сообщение</div>
                        <p>asdasd</p>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
    	</div>
    </div>
    <style>
        .clear {
            clear: both;
        }
        .dialog {
            width: 90%;
            margin: 0 auto;
        }
        .dialog .message {
            width: 100%;
            margin: 15px 0px;
        }
        .dialog .message .avatar {
            width: 6%;
        }
        .dialog .message .avatar img {
            width: 60px;
            height: 60px;
            border: 1px solid #27da93;
            border-radius: 30px;
            margin-bottom: 5px;
        }
        .dialog .message .avatar p {
            font-size: 11px;
            text-align: center;
        }
        .dialog .message .text {
            padding: 20px;
            border: 1px solid #27da93;
            border-radius: 10px;
            width: 93%;
        }
        .dialog .message .text .author {
            font-weight: bold;
        }
        .dialog .message.left .avatar {
            float: left;
        }
        .dialog .message.left .text {
            float: right;
        }
        .dialog .message.right .avatar {
            float: right;
        }
        .dialog .message.right .text {
            background-color: rgba(39, 218, 147, 0.5);
            float: left;
        }
    </style>
@stop