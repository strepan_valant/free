<ul class="nav nav-pills" role="tablist">
    <li role="presentation"{{ $page == 'ads' ? ' class=active' : '' }}>
        <a href="/profile/ads">Мои объявления</a>
    </li>
    <li role="presentation"{{ $page == 'messages' ? ' class=active' : '' }}>
        <a href="/profile/messages">Сообщения<span class="badge">3</span></a>
    </li>
    <li role="presentation"{{ $page == 'favorites' ? ' class=active' : '' }}>
        <a href="/profile/favorites">Избранное</a>
    </li>
    <li role="presentation"{{ $page == 'profile' ? ' class=active' : '' }}>
        <a href="/profile">Профиль</a>
    </li>
    <li role="presentation"{{ $page == 'contacts' ? ' class=active' : '' }}>
        <a href="/profile/contacts">Контакты</a>
    </li>
</ul>
<hr>