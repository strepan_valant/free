@extends('layouts.main')

@section('title', 'Мой профиль')

@section('content')
    @include('main.partials.header', ['title' => 'Мой профиль'])

    <div class="profile">
    	<div class="container">
			@include('profile.partials.profile-menu', ['page' => 'contacts'])
    		<div class="loan-col">
    			<h3>Контакты</h3>
				@if(Session::has('error-profile') && $errors->any())
					<div class="alert alert-danger">
					    @foreach($errors->all() as $error)
					        <div>{{ $error }}</div>
					    @endforeach
					</div>
                @elseif(Session::has('success-profile'))
                    <div class="alert alert-success">
                        {{ Session::get('success-profile') }}
                    </div>
				@endif
				{!! Form::model($profile, array('url' => 'profile/contacts')) !!}
				<div class="col-loan">
					<ul class="loan-col1">
						<li>
							{!! Form::label('city_id', 'Город') !!}
						</li>
						<label>:</label>
						<li class="loan-list-top">
							{!! Form::text('city_id', $profile->city_id, [
								'data-id' => $profile->city_id,
								'data-name' => $profile->city ? $profile->city->name : '',
								'data-region' => $profile->city ? $profile->city->region->name : '',
								'name' => 'Profile[city_id]',
								'class' => 'cities-ajax-select'
							]) !!}
						</li>
					</ul>
					<ul class="loan-col1">
						<li>
							{!! Form::label('phone', 'Телефон') !!}
						</li>
						<label>:</label>
						<li class="loan-list-top">
							{!! Form::text('phone', $profile->phone, ['name' => 'Profile[phone]']) !!}
						</li>
					</ul>
					<ul class="loan-col1">
						<li>
							{!! Form::label('skype', 'Skype') !!}
						</li>
						<label>:</label>
						<li class="loan-list-top">
							{!! Form::text('skype', $profile->skype, ['name' => 'Profile[skype]']) !!}
						</li>
					</ul>
					<div class="sub">
						<label class="hvr-sweep-to-right">
							{!! Form::submit('Сохранить') !!}
						</label>
					</div>
				</div>
				{!! Form::close() !!}
    		</div>
    	</div>
    </div>
@stop