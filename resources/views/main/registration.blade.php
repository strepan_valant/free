@extends('layouts.main')

@section('title', 'Регистрация')

@section('content')
    @include('main.partials.header', ['title' => 'Регистрация])

    <div class="login-right">
        <div class="container">
            <h3>Регистрация</h3>
            <div class="login-top">
                <ul class="login-icons">
                    <li><a href="#"><i class="facebook"> </i><span>Facebook</span></a></li>
                    <li><a href="#" class="twit"><i class="twitter"></i><span>Twitter</span></a></li>
                    <li><a href="#" class="go"><i class="google"></i><span>Google +</span></a></li>
                    <li><a href="#" class="in"><i class="linkedin"></i><span>Vkontakte</span></a></li>
                    <div class="clearfix"> </div>
                </ul>
                <div class="form-info">
                    <form>
                        <table>
                            <tr>
                                <td>Логин: </td>
                                <td><input type="text" placeholder="Логин" required=""></td>
                            </tr>
                            <tr>
                                <td>Email: </td>
                                <td><input type="text" placeholder="Email" required=""></td>
                            </tr>
                            <tr>
                                <td>Пароль: </td>
                                <td><input type="password" placeholder="Пароль" required=""></td>
                            </tr>
                            <tr>
                                <td>Имя: </td>
                                <td><input type="text" placeholder="Имя" required=""></td>
                            </tr>
                            <tr>
                                <td>Фамилия: </td>
                                <td><input type="text" placeholder="Фамилия" required=""></td>
                            </tr>
                            <tr>
                                <td>Дата рождения: </td>
                                <td><input type="date" placeholder="Дата рождения" required=""></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <label class="hvr-sweep-to-right" style="margin: 0 auto; display: table;">
                                        <input type="submit" value="Зарегистрироваться">
                                    </label>
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    </form>
                    <p>Уже зарегистрированы? <a href="/login">Войти</a></p>
                </div>
            </div>
        </div>
    </div>
@stop