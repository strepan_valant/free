@extends('layouts.main')

@section('title', 'Поиск')

@section('content')
    @include('main.partials.search', ['title' => 'Поиск'])
    @include('main.partials.categories')

    <div class="dealers">
        <div class="container">
            <div class="single-buy">
                @foreach($search->filters as $block_name => $filter_values)
                <div class="col-sm-3 check-top-single">
                    <div class="single-bottom">
                        <h4>{{ $block_name }}</h4>
                        <ul>
                            @foreach($filter_values as $filter_key => $filter_value)
                            <li>
                                <input type="checkbox" id="{{ $filter_value }}" value="1" name="{{ $filter_key }}" {{ ($search->{$filter_key}) ? 'checked' : '' }}>
                                <label for="{{ $filter_value }}"><span></span> {{ $filter_value }}</label>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endforeach
                <div class="clearfix"></div>
            </div>
            <div class="dealer-top">
                <h4>Результаты поиска</h4>
                @foreach($search->getAds() as $ad)
                <div class="col-md-3 top-deal-top">
                    <div class=" top-deal">
                        <a href="/ad/{{ $ad->alias }}" class="mask"><img src="/images/de.jpg" class="img-responsive zoom-img" alt=""></a>
                        <div class="deal-bottom">
                            <div class="top-deal1">
                                <h5><a href="/ad/{{ $ad->alias }}">{{ $ad->title }}</a></h5>
                                <p>Город : {{ $ad->city->name }}</p>
                                <p>Тип: {{ $ad->getAdTypes() }}</p>
                            </div>
                            <div class="top-deal2">
                                <a href="/ad/{{ $ad->alias }}" class="hvr-sweep-to-right more">Подробнее</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>

@stop