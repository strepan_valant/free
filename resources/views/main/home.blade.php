@extends('layouts.main')

@section('title', 'Главная')

@section('content')
    @include('main.partials.slider')
    @include('main.partials.categories', ['q' => ''])
@stop