<div class="banner-bottom-top">
    <div class="container">
        <div class="bottom-header">
            <div class="header-bottom categories">
                @foreach(App\Category::getCategories() as $category)
                <div class=" bottom-head {{ (isset($alias) && $alias == $category->alias ? 'active' : '') }}">
                    @if(isset($alias) && $alias == $category->alias)
                    <a href="/search{{ ($search->query) ? "?q=$search->query" : '' }}">
                    @else
                    <a href="/category/{{ $category->alias . ((isset($search) && $search->query) ? "?q=$search->query" : '') }}">
                    @endif
                        <div class="buy-media">
                            <img src="\img\categories\{{ $category->alias }}.png">
                            <h6>{{ $category->name }}</h6>
                        </div>
                    </a>
                </div>
                @endforeach
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>