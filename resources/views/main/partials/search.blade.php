<main>
    <div class="banner-buying">
        <div class="search">
            <form method="get" action="{{ $_SERVER["REDIRECT_URL"] }}">
                <input type="text" name="q" placeholder="{{ $search->error }}" value="{{ $search->query }}">
                <button>ПОИСК</button>
            </form>
        </div>
    </div>
</main>