@extends('layouts.main')

@section('title', 'Авторизация')

@section('content')
    @include('main.partials.header', ['title' => 'Авторизация'])

    <div class="login-right">
        <div class="container">
            <h3>Авторизация</h3>
            <div class="login-top">
                <ul class="login-icons">
                    <li><a href="/auth/facebook"><i class="facebook"> </i><span>Facebook</span></a></li>
                    <li><a href="/auth/twitter" class="twit"><i class="twitter"></i><span>Twitter</span></a></li>
                    <li><a href="/auth/google" class="go"><i class="google"></i><span>Google +</span></a></li>
                    <li><a href="/auth/vk" class="in"><i class="linkedin"></i><span>Vkontakte</span></a></li>
                    <div class="clearfix"> </div>
                </ul>
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{ Session::get('error') }}
                    </div>
                @endif
                <div class="form-info">
                    <form method="post">
                        <table>
                            <tr>
                                <td>Email или Логин: </td>
                                <td><input name="email" type="text" class="text" placeholder="Email или Логин" required=""></td>
                            </tr>
                            <tr>
                                <td>Пароль: </td>
                                <td><input name="password" type="password" placeholder="Пароль" required=""></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <label class="hvr-sweep-to-right" style="margin: 0 auto; display: table;">
                                        <input type="submit" value="Войти">
                                    </label>
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    </form>
                </div>
                <div class="create">
                    <h4>Вы новый пользователь?</h4>
                    <a class="hvr-sweep-to-right" href="/registration">Зарегистрироваться</a>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
@stop