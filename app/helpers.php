<?php

function getYesOrNo($var)
{
    if ($var) {
        return 'Да';
    } else {
        return 'Нет';
    }
}