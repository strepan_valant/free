<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

    protected $table = 'cities';
    public $timestamps = false;

    public function region()
    {
        return $this->hasOne('App\Region', 'id', 'region_id');
    }

    public static function getCitiesList()
    {
        return City::lists('name', 'id');
    }

}
