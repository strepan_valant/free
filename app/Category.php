<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Cache;

class Category extends Model {

    protected $table = 'categories';
    public $timestamps = false;

    public static function getCategories()
    {
        return Cache::rememberForever('categories', function()
        {
            return self::get();
        });
    }

}
