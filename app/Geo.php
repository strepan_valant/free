<?php
namespace App;

use Curl;
use App\City;

class Geo {

    public static function location($request, $response)
    {
        if ($request->cookie('location')) {
            return $response;
        }
        $ip = ($request->ip() != '127.0.0.1') ? $request->ip() : '77.120.137.175';
        $ipinfo = json_decode(Curl::to("http://ipinfo.io/$ip/json")->get());
        if ($ipinfo->ip && $ipinfo->loc) {
            $geocode = json_decode(Curl::to("http://maps.googleapis.com/maps/api/geocode/json?latlng=$ipinfo->loc&address=$ipinfo->city&language=ru")->get());
            if ($geocode && isset($geocode->results) &&
                is_array($geocode->results) &&
                isset($geocode->results[0]) &&
                is_array($geocode->results[0]->address_components) &&
                isset($geocode->results[0]->address_components) &&
                isset($geocode->results[0]->address_components[0]->long_name)
            ) {

                $city_name = $geocode->results[0]->address_components[0]->long_name;
                $state_name = $geocode->results[0]->address_components[2]->long_name;
                $city = City::where('name', $city_name)->whereHas('region', function($q) use ($state_name) {
                    $q->where('name', $state_name);
                })->first();
                if ($city) {
                    return $response->withCookie(cookie('location', $city->id, 43200));
                }

            }
        }
        return $response;
    }

}