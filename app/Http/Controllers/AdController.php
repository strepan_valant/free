<?php
namespace App\Http\Controllers;

use App\Ad;

class AdController extends Controller
{
    public function index($alias)
    {
        if ($ad = Ad::where('alias', $alias)->first()) {
            return view('ad/index', ['ad' => $ad]);
        } else {

        }
    }
}
