<?php
namespace App\Http\Controllers;

use App\Ad;
use App\City;
use App\Profile;
use App\User;
use Auth, Hash;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public $user;
    public $request;

    public function __construct(Request $request)
    {
        $this->user = Auth::user();
        $this->request = $request;
        /*$this->middleware('guest');*/
    }

    public function getProfile()
    {
        return view('profile/profile', [
            'user'    => $this->user,
            'profile' => $this->user->profile ? $this->user->profile : new Profile
        ]);
    }

    public function postProfile()
    {
        if ($User = $this->request->get('User')) {

            if ($User['password']) {
                $User['password'] = Hash::make($User['password']);
            } else {
                unset($User['password']);
            }
            $user = User::find($this->user->id);
            return $user->store($User);

        }
    }

    public function getContacts()
    {
        return view('profile/contacts', [
            'user'    => $this->user,
            'profile' => $this->user->profile ? $this->user->profile : new Profile,
            'cities'  => City::getCitiesList()
        ]);
    }

    public function postContacts()
    {
        if ($Profile = $this->request->get('Profile')) {

            if ($this->user->profile) {
                $profile = Profile::where('user_id', $this->user->id)->first();
            } else {
                $profile = new Profile;
                $profile->user_id = $this->user->id;
            }
            return $profile->store($Profile);

        }
    }

    public function favorites()
    {
        return view('profile/favorites', [
            'user'    => $this->user,
            'profile' => $this->user->profile ? $this->user->profile : new Profile
        ]);
    }

    public function ads()
    {
        return view('profile/ads/index', [
            'user'    => $this->user,
            'profile' => $this->user->profile ? $this->user->profile : new Profile
        ]);
    }

    public function addAd()
    {
        return view('profile/ads/add', [
            'user'    => $this->user,
            'profile' => $this->user->profile ? $this->user->profile : new Profile,
            'ad'      => new Ad
        ]);
    }

    public function editAd($id)
    {
        if ($ad = Ad::where('user_id', $this->user->id)->find($id)) {

            return view('profile/ads/edit', [
                'user'    => $this->user,
                'profile' => $this->user->profile ? $this->user->profile : new Profile,
                'ad'      => $ad
            ]);
        }
    }

    public function saveAd($id = null)
    {
        $Ad = $this->request->get('Ad');
        if ($id) {
            $ad = Ad::find($id);
        } else {
            $ad = new Ad;
        }
        $ad->prepare();
        return $ad->store($Ad);
    }
}
