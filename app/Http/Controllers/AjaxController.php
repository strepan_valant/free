<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, AWS, Datatable, DB, File, Image, Input;
use App\City;
use App\Ad;

class AjaxController extends Controller
{
    public $user;
    public $action;
    public $data;

    public function __construct(Request $request)
    {
        $this->user   = Auth::user();
        $this->action = $request->input('action');
        $this->data   = $request->input('data');
    }

    public function index()
    {
        switch ($this->action) {
            case 'get_cities':
                $return = $this->getCities($this->data);
                break;
            case 'get_user_ads':
                $return = $this->getUserAds($this->data);
                break;
            default:
                break;
        }

        $this->results($return);
    }

    function results($return) {
        if (!is_array($return)) {
            $data['message'] = $return;
        } else {
            $data = $return;
        }

        echo json_encode(array_merge($this->_errorArray(), $data));

        exit;
    }

    protected function _errorArray() {
        return [
            'status'  => 'error',
            'data'    => [],
            'message' => 'error'
        ];
    }

    function getCities($data)
    {
        $q = $data['q'];

        $cities = City::leftJoin('regions', function($join) {
            $join->on('regions.id', '=', 'cities.region_id');
        })
            ->where('cities.name', 'LIKE', "$q%")
            ->select('cities.id as id', 'cities.name as name', 'regions.name as region')
            ->get()->toArray();

        return [
            'status'  => 'success',
            'data'    => $cities,
            'message' => 'success'
        ];
    }

    function getUserAds($data)
    {
        $ads = Ad::where('user_id', $this->user->id)
            ->select(DB::raw('DATE_FORMAT(updated_at, \'%d %M, %Y\') AS date'), 'title', 'status', 'id', 'alias')
            ->select('updated_at AS date', 'title', 'status', 'id', 'alias')
            ->get();

        return [
            'status'  => 'success',
            'data'    => $ads ? $ads->toArray() : [],
            'message' => 'success'
        ];
    }

    public function changeAvatar()
    {
        $file = Input::file('avatar');
        $user = Auth::user();
        if ($user && $file) {
            if ($file->getClientSize() > 2097152) {
                echo json_encode([
                    'status'  => 'error',
                    'data'    => [],
                    'message' => 'Максимальный размер файла 2MB!'
                ]);
            } else {
                if (File::exists(public_path() . "/uploads/users/avatars/{$user->id}.jpg")) {
                    File::delete(public_path() . "/uploads/users/avatars/{$user->id}.jpg");
                }
                $file->move(public_path() . '/uploads/users/avatars/', "{$user->id}.jpg");
                $img = Image::make(public_path() . "/uploads/users/avatars/{$user->id}.jpg")->resize(271, 271);
                $img->save(public_path() . "/uploads/users/avatars/{$user->id}.jpg", 100);
                echo json_encode([
                    'status'  => 'success',
                    'data'    => [
                        'url' => "/uploads/users/avatars/{$user->id}.jpg"
                    ],
                    'message' => 'success'
                ]);
            }
        }
    }

    public function addAdPhoto()
    {
        if ($this->user && Input::file('image')) {
            $image = Input::file('image');
            $s3 = AWS::get('s3');
            $file = $s3->putObject(array(
                'Bucket'     => 'strepanfree',
                'Key'        => sha1(microtime(true)) . "_{$this->user->id}." . $image->getClientOriginalExtension(),
                'SourceFile' => $image->getPathName(),
            ));
            if ($file) {
                echo json_encode([
                    'status'  => 'success',
                    'data'    => [
                        'url' => $file['ObjectURL']
                    ],
                    'message' => 'success'
                ]);
            }
        }
    }

}