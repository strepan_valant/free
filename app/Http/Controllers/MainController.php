<?php
namespace App\Http\Controllers;

use App\Category;
use App\Search;
use AWS, Auth, Hash;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public $user;
    public $request;

	public function __construct(Request $request)
	{
        $this->user = Auth::user();
        $this->request = $request;
		/*$this->middleware('guest');*/
	}

	public function index()
	{
        return view('main/home', [
		]);
	}

	public function login()
	{
		return view('main/login', [
		]);
	}

	public function registration()
	{
		return view('main/registration', [
		]);
	}

    public function search()
    {
        return view('main/search', [
            'search' => (new Search($this->request->get('q')))
        ]);
    }

    public function category($alias)
    {
        if ($category = Category::where('alias', $alias)->first()) {
            $search = new Search($this->request->get('q'));
            $search->category = $category;
            return view('main/search', [
                'search'  => $search,
                'alias'   => $alias
            ]);
        }
    }
}
