<?php
namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function __construct(Request $request)
    {
        $this->user = Auth::user();
        $this->request = $request;
    }

    public function index()
    {
        return view('profile/messages/index', [
            'user'    => $this->user,
            'profile' => $this->user->profile ? $this->user->profile : new \App\Profile
        ]);
    }

    public function dialog($from_user_id, $to_user_id)
    {
        return view('profile/messages/dialog', [
            'user'    => $this->user,
            'profile' => $this->user->profile ? $this->user->profile : new \App\Profile
        ]);
    }

    public function deleteDialog($from_user_id, $to_user_id)
    {

    }

}