<?php
namespace App\Http\Controllers;

use Auth;
use Input;
use Session;
use Hash;
use Request;
use App\User;

class AuthController extends Controller
{
    public function __construct()
    {
        /*$this->middleware('guest');*/
    }

    public function login()
    {
        $user = User::where('email', Input::get('email'))->first();

        if ($user && Hash::check(Input::get('password'), $user->password)) {
            Auth::login($user, false);
            return redirect('/');
        } else {
            Session::flash('error', 'Your email and/or password is incorrect');
            return redirect('login');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    public function loginWithGoogle(Request $request)
    {
        $code = $request->get('code');
        $googleService = \OAuth::consumer('Google');

        if (!is_null($code)) {
            $token = $googleService->requestAccessToken($code);
            $result = json_decode($googleService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);
            $message = 'Your unique Google user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
            echo $message. "<br/>";
            dd($result);
        } else {
            $url = $googleService->getAuthorizationUri();
            return redirect((string)$url);
        }
    }

    public function loginWithFacebook(Request $request)
    {
        $code = $request->get('code');
        $fb = \OAuth::consumer('Facebook');

        if (!is_null($code)) {
            $token = $fb->requestAccessToken($code);
            $result = json_decode($fb->request('/me'), true);
            $message = 'Your unique facebook user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
            echo $message. "<br/>";
            dd($result);
        } else {
            $url = $fb->getAuthorizationUri();
            return redirect((string)$url);
        }
    }

    public function loginWithTwitter(Request $request)
    {
        $token  = $request->get('oauth_token');
        $verify = $request->get('oauth_verifier');
        $tw = \OAuth::consumer('Twitter');

        if (!is_null($token) && ! is_null($verify))
        {
            $token = $tw->requestAccessToken($token, $verify);
            $result = json_decode($tw->request('account/verify_credentials.json'), true);
            $message = 'Your unique Twitter user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
            echo $message. "<br/>";
            dd($result);
        } else {
            $reqToken = $tw->requestRequestToken();
            $url = $tw->getAuthorizationUri(['oauth_token' => $reqToken->getRequestToken()]);
            return redirect((string)$url);
        }
    }

    public function loginWithVk(Request $request)
    {

    }
}
