<?php

Route::get('/', ['middleware' => 'geo', 'uses' => 'MainController@index']);

Route::group(['prefix' => 'ajax', 'middleware' => 'ajax'], function() {
    Route::any('/', ['as' => 'ajax', 'uses' => 'AjaxController@index']);
    Route::any('changeAvatar', 'AjaxController@changeAvatar');
    Route::any('addAdPhoto', 'AjaxController@addAdPhoto');
});

Route::group(['prefix' => 'profile', 'middleware' => 'auth'], function() {
    Route::get('/', 'ProfileController@getProfile');
    Route::post('/', 'ProfileController@postProfile');
    Route::get('favorites', 'ProfileController@favorites');
    Route::get('contacts', 'ProfileController@getContacts');
    Route::post('contacts', 'ProfileController@postContacts');

    Route::group(['prefix' => 'ads'], function() {
        Route::get('/', 'ProfileController@ads');
        Route::get('add', 'ProfileController@addAd');
        Route::post('add', 'ProfileController@saveAd');
        Route::get('{id}/edit', 'ProfileController@editAd');
        Route::post('{id}/edit', 'ProfileController@saveAd');
    });

    Route::group(['prefix' => 'messages'], function() {
        Route::get('/', 'MessageController@index');
        Route::get('{from_user_id}/{to_user_id}', 'MessageController@dialog');
        Route::post('{from_user_id}/{to_user_id}/delete', 'MessageController@deleteDialog');
    });
});

Route::get('login', ['middleware' => 'guest', 'uses' => 'MainController@login']);
Route::post('login', ['middleware' => 'guest', 'uses' => 'AuthController@login']);
Route::get('logout', ['middleware' => 'auth', 'uses' => 'AuthController@logout']);
Route::get('search', ['uses' => 'MainController@search']);
Route::get('category/{alias}', ['uses' => 'MainController@category']);

Route::get('registration', ['middleware' => 'guest', 'uses' => 'MainController@registration']);
Route::post('registration', ['middleware' => 'guest', 'uses' => 'AuthController@registration']);

Route::group(['prefix' => 'auth', 'middleware' => 'guest'], function() {
    Route::any('google', 'AuthController@loginWithGoogle');
    Route::any('facebook', 'AuthController@loginWithFacebook');
    Route::any('twitter', 'AuthController@loginWithTwitter');
    Route::any('vk', 'AuthController@loginWithVk');
});

Route::group(['prefix' => 'ad'], function() {
    Route::get('{alias}', 'AdController@index');
});