<?php
namespace App\Http\Middleware;

use Closure;
use App\Geo;

class GeoMiddleware {

    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $response = Geo::location($request, $response);

        return $response;
    }
}