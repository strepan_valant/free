<?php
namespace App\Http\Middleware;

use Closure;

class AjaxMiddleware {

    public function handle($request, Closure $next)
    {
        if (!$request->ajax()) {
            return response('Error', 401);
        }
        return $next($request);
    }
}