<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Cookie, Validator;

class Profile extends Model {

    protected $table = 'user_profile';
    public $timestamps = false;

    protected $guarded = [
        'user_id'
    ];

    public $attributesNames = [
        'user_id' => 'Пользователь',
        'city_id' => 'Город',
        'phone'   => 'Телефон'
    ];

    public $rules = [
        'user_id' => 'required|integer',
        'city_id' => 'required|integer',
        'phone'   => 'required|min:13'
    ];

    public $rulesMessages = [
        'required' => 'Заполните поле :attribute',
        'integer'  => 'Неправильно заполнено поле :attribute',
        'min'      => 'Минимальное значение поля :attribute - :min символов'
    ];

    public function city()
    {
        return $this->hasOne('App\City', 'id', 'city_id');
    }

    public function __construct()
    {
        if ($city_id = Cookie::get('location')) {
            $this->city_id = $city_id;
        }
    }

    public function store($Profile)
    {
        $this->fill($Profile);
        $validator = Validator::make($this->toArray(), $this->rules, $this->rulesMessages)->setAttributeNames($this->attributesNames);
        if (!$validator->fails() && $this->save()) {
            return redirect()->back()->with('success-profile', 'Ваши контакты были успешно сохранены!');
        } else {
            return redirect()->back()->with('error-profile', true)->withErrors($validator);
        }
    }

}
