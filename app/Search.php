<?php
namespace App;

use App\Ad;

class Search {

    public $query;
    public $error;
    public $is_gift = 1;
    public $is_no_gift = 0;
    public $is_to_exchange = 0;
    public $new = 1;
    public $used = 1;
    public $category;

    public function __construct($query)
    {
        $this->query = $query;
        if (!$this->query) {
            $this->error = 'Введите ключевое слово для поиска';
        }
    }

    public $filters = [
        'Тип' => [
            'is_gift' => 'Отдам',
            'is_no_gift' => 'Приму'
        ],
        'Обмен' => [
            'is_to_exchange' => 'Обменяю'
        ],
        'Состояние' => [
            'new'  => 'Новое',
            'used' => 'Б/у',
        ]
    ];

    public function getFilters()
    {
        return $this->filters;
    }

    public function getAds()
    {
        return Ad::getAds($this->query, $this->category);
    }

}