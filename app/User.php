<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Validator;
use App\Profile;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

    protected $table = 'users';
    public $timestamps = false;
	protected $fillable = ['login', 'email', 'password', 'first_name', 'last_name', 'birthday', 'has_avatar'];

    public $attributesNames = [
        'login'      => 'Логин',
        'email'      => 'E-mail',
        'password'   => 'Пароль',
        'first_name' => 'Имя',
        'last_name'  => 'Фамилия',
        'birthday'   => 'День рождения',
        'has_avatar' => 'Аватар'
    ];

    public $rules = [
        'login'      => 'required',
        'email'      => 'required|email|unique:users,email',
        'password'   => 'required|min:6',
        'first_name' => 'required',
        'last_name'  => 'required',
        'birthday'   => 'required|date',
        'has_avatar' => 'required'
    ];

    public $rulesMessages = [
        'required' => 'Заполните поле :attribute',
        'integer'  => 'Неправильно заполнено поле :attribute',
        'min'      => 'Минимальное значение поля :attribute - :min символов'
    ];

    public function profile()
    {
        return $this->hasOne('App\Profile', 'user_id', 'id');
    }

    public function getRememberToken()
    {
        return null;
    }

    public function setRememberToken($value)
    {
        // not supported
    }

    public function getRememberTokenName()
    {
        return null;
    }

    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute)
        {
            parent::setAttribute($key, $value);
        }
    }

    public function store($User)
    {
        $this->fill($User);
        $this->rules['email'] .= ",{$this->id}";
        $validator = Validator::make($this->toArray(), $this->rules, $this->rulesMessages)->setAttributeNames($this->attributesNames);
        if (!$validator->fails() && $this->save()) {
            return redirect()->back()->with('success-user', 'Ваш профиль был успешно сохранен!');
        } else {
            return redirect()->back()->with('error-user', true)->withErrors($validator);
        }
    }

    public function getAvatar()
    {
        return $this->has_avatar ? "/uploads/users/avatars/{$this->id}.jpg" : "/images/avatar.png";
    }

}
