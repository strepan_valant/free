<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth, DB, Input, Slug, Validator;
use App\File;

class Ad extends Model
{
    protected $table = 'ads';

    protected $fillable = ['title', 'description', 'category_id', 'condition', 'is_gift', 'is_to_exchange', 'is_to_pickup', 'pickup_address', 'city_id'];

    const conditionUsed = 0;
    const conditionNew = 1;

    static $conditions = [
        self::conditionUsed => 'Б/у',
        self::conditionNew => 'Новый'
    ];

    const statusDeleted = 0;
    const statusActive = 1;
    const statusClosed = 2;
    const statusPaused = 3;

    static $statuses = [
        self::statusDeleted => 'Удалено',
        self::statusActive => 'Активно',
        self::statusClosed => 'Закрыто',
        self::statusPaused => 'Приостановлено'
    ];

    const isToGiveLabel = 'Отдам';
    const isToTakeLabel = 'Приму';
    const isToExchangeLabel = 'Обменяю';

    public $attributesNames = [
        'city_id' => 'Город',
        'category_id' => 'Категория',
        'title' => 'Заголовок',
        'description' => 'Описание'
    ];

    public $rules = [
        'city_id' => 'required',
        'category_id' => 'required',
        'title' => 'required|min:4',
        'description' => 'required'
    ];

    public $rulesMessages = [
        'required' => 'Заполните поле :attribute',
        'min' => 'Минимальное значение поля :attribute - :min символа'
    ];

    public function __construct()
    {
        $user = Auth::user();
        if ($user && $user->profile) {
            $this->city_id = $user->profile->city_id;
        }
        $this->is_gift = 1;
        $this->condition = self::conditionUsed;
        $this->is_to_exchange = 0;
        $this->is_to_pickup = 1;
    }

    public function city()
    {
        return $this->hasOne('App\City', 'id', 'city_id');
    }

    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function images()
    {
        return $this->hasMany('App\File', 'ad_id');
    }

    public function prepare()
    {
        $this->user_id = Auth::user()->id;
        $this->status = 1;
        return $this;
    }

    public function store($Ad)
    {
        $this->fill($Ad);
        if (!$this->alias) {
            $this->generateAlias();
        }
        $validator = Validator::make($this->toArray(), $this->rules, $this->rulesMessages)->setAttributeNames($this->attributesNames);
        if (!$validator->fails() && $this->save()) {
            $this->savePhotos();
            return redirect('profile/ads')->with('success-profile', 'Ваше объявление было успешно ' . ($this->id ? 'сохранено' : 'размещено') . '!');
        } else {
            return redirect()->back()->with('error-profile', true)->withErrors($validator);
        }
    }

    private function savePhotos()
    {
        foreach (Input::get('Images')['new'] as $link) {
            if ($link) {
                $file = new File;
                $file->user_id = $this->user_id;
                $file->ad_id = $this->id;
                $file->type = 'Ad';
                $file->link = $link;
                $file->save();
            }
        }
        return true;
    }

    public function getStatusAttribute($value)
    {
        return self::$statuses[$value];
    }

    public function getConditionAttribute($value)
    {
        return self::$conditions[$value];
    }

    public static function getAds($where, $category)
    {
        $ads = self::where('title', 'LIKE', "%$where%");
        if ($category) {
            $ads = $ads->where('category_id', $category->id);
        }
        $ads = $ads->get();
        return $ads;
    }

    public function getAdTypes()
    {
        $types = [];
        if ($this->is_gift) {
            $types[] = strtolower(self::isToGiveLabel);
        }
        if (!$this->is_gift) {
            $types[] = strtolower(self::isToTakeLabel);
        }
        if ($this->is_to_exchange) {
            $types[] = strtolower(self::isToExchangeLabel);
        }
        return implode(', ', $types);
    }

    public function generateAlias()
    {
        return $this->alias = Slug::make($this->title);
    }

}
