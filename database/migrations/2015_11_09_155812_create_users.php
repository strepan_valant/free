<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('login', 255);
            $table->string('password', 255);
            $table->string('email', 255);
            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->date('birthday');
        });

        Schema::create('user_social', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('social', 255);
            $table->string('sid', 20);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('users');
        Schema::drop('user_social');
	}

}
