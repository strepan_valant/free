<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('city_id');
            $table->integer('category_id');
            $table->string('alias');
            $table->string('title');
            $table->text('description');
            $table->boolean('is_gift')->default(1);
            $table->boolean('is_to_exchange')->default(0);
            $table->boolean('is_to_pickup')->default(1);
            $table->integer('condition')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });

        /*DB::unprepared("INSERT INTO `free`.`ads` (`id`, `user_id`, `city_id`, `category_id`, `alias`, `title`, `description`, `is_gift`, `is_to_exchange`, `condition`, `stars`, `status`, `created_at`, `updated_at`) VALUES (NULL, '2', '29', '9', 'stulchik', 'Стульчик', 'Стульчик детский 50х20', '1', '0', '0', '0.00', '1', '2015-11-23 00:00:00', '2015-11-23 00:00:00');");*/
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('ads');
	}

}
