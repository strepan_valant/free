<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        DB::table('categories')->delete();

        DB::table('categories')->insert(['name' => 'Детское', 'alias' => 'detskoe']);
        DB::table('categories')->insert(['name' => 'Недвижимость', 'alias' => 'nedvizhimost']);
        DB::table('categories')->insert(['name' => 'Транспорт', 'alias' => 'transport']);
        DB::table('categories')->insert(['name' => 'Животные', 'alias' => 'zhivotnie']);
        DB::table('categories')->insert(['name' => 'Дом и сад', 'alias' => 'dom_i_sad']);
        DB::table('categories')->insert(['name' => 'Мебель и интерьер', 'alias' => 'mebel']);
        DB::table('categories')->insert(['name' => 'Техника и электроника', 'alias' => 'elektronika']);
        DB::table('categories')->insert(['name' => 'Одежда', 'alias' => 'odezhda']);
        DB::table('categories')->insert(['name' => 'Хобби и отдых', 'alias' => 'hobbi_i_otdikh']);
        DB::table('categories')->insert(['name' => 'Спорт', 'alias' => 'sport']);
        DB::table('categories')->insert(['name' => 'Еда', 'alias' => 'eda']);
        DB::table('categories')->insert(['name' => 'Оборудование и инструменты', 'alias' => 'oborudovanie']);
        DB::table('categories')->insert(['name' => 'Разное', 'alias' => 'raznoe']);
        DB::table('categories')->insert(['name' => 'Нужна помощь', 'alias' => 'pomosch']);
    }
}
