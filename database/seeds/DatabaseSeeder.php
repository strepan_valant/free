<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// $this->call('UserTableSeeder');

		//Categories
		$this->call('CategoriesSeeder');
		$this->command->info('Seeded the categories!');

		//Users
		$this->call('UsersSeeder');
		$this->command->info('Seeded the users!');
    }

}
