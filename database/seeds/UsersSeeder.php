<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'login' => 'admin',
            'password' => '$2y$10$7OcrB2FEkjTpLwo9S2wcY.fAOk6O9X2rKELk9yf1lqECLoBHBqdiC',
            'email' => 'admin@mail.ru',
            'first_name' => 'admin',
            'last_name' => 'admin',
            'birthday' => '1989-08-05'
        ]);
    }
}
