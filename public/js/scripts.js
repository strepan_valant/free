$(window).load(function () {
    $('.nav_slide_button').click(function () {
        $('.pull').slideToggle();
    });

    $('.flexslider').flexslider({
        animation: "slide",
        controlNav: "thumbnails"
    });
});

$(document).ready(function(){

    $(".fancybox").fancybox();

    $('#is_to_pickup').on('click', function(){
        if (this.checked) {
            $('#pickup_address').prop("disabled", false);
        } else {
            $('#pickup_address').prop("disabled", true).val('');
        }
    });

    $('.cities-ajax-select').select2({
        initSelection: function (element, callback) {
            callback({
                id: $('input.cities-ajax-select').data('id'),
                name: $('input.cities-ajax-select').data('name'),
                region: $('input.cities-ajax-select').data('region')
            });
        },
        ajax: {
            url: "/ajax",
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    action: 'get_cities',
                    data: {
                        q: term
                    }
                };
            },
            results: function (data, page) {
                return { results: data.data };
            },
            cache: true
        },
        minimumInputLength: 2,
        formatNoMatches: function () {
            return "Ничего не найдено";
        },
        formatAjaxError: function () {
            return "Ошибка загрузки";
        },
        formatSearching: function () {
            return "Поиск...";
        },
        formatInputTooShort: function () {
            return "Введите минимум 2 символа";
        },
        formatResult: function (item) {
            return $('<option value="' + item.id + '">' + item.name + ' (' + item.region + ')</option>');
        },
        formatSelection: function (item) {
            return item.name + ' (' + item.region +')';
        },
        escapeMarkup: function (m) { return m; }
    });

    $('.categories-select').select2({
        formatNoMatches: function () {
            return "Ничего не найдено";
        }
    });

    $('#change-avatar').on('click', function(){
        $('#profile-left input[type=file]').trigger('click');
    });

    $('.profile-left .fileupload').fileupload({
        dataType: 'json',
        url: '/ajax/changeAvatar',
        add: function (e, data) {
            data.submit();
        },
        done: function (e, data) {
            if (data.result.status == 'success') {
                $('#profile-left img').attr('src', data.result.data.url + '?' +  + new Date().getTime());
                $('#change-avatar').html('Изменить');
                $('#remove-avatar').show();
                $('#has_avatar').val(1);
                $('.alerts').html('<div class="alert alert-info">Ваш аватар был изменен. Не забудьте сохранить ваш профиль!</div>');
            } else if (data.result.status == 'error') {
                $('.alerts').html('<div class="alert alert-danger">' + data.result.message + '</div>');
            }
        }
    });

    $('#remove-avatar').on('click', function() {
        $('#profile-left img').attr('src', '/images/avatar.png');
        $(this).hide();
        $('#change-avatar').html('Добавить');
        $('#has_avatar').val(0);
        $('.alerts').html('<div class="alert alert-info">Ваш аватар был удален. Не забудьте сохранить ваш профиль!</div>');
    });

    $('#user-ads').DataTable({
        ajax: {
            url: '/ajax',
            type: 'POST',
            data: {
                action: 'get_user_ads'
            }
        },
        "columns": [
            { "data": "date", "width": "14%",
                "title": "Дата",
                "render": function(data, type, row) {
                    var monthNames = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"];
                    var date = new Date(data);
                    return date.getDate() + ' ' + monthNames[date.getMonth()] + ', ' + date.getFullYear();
                }
            },
            { "data": "title", "title": "Заголовок" },
            { "data": "status", "title": "Статус", "width": "9%" },
            { "data": "id", "title": "", "width": "160px", "orderable": false,
                "type": "html",
                "render": function(data, type, row) {
                    return '' +
                    ' <img title="Просмотреть" src="/img/grid/view.png" onclick="window.open(\'/ad/' + row.alias + '\', \'_blank\')">' +
                    ' <img title="Редактировать" src="/img/grid/edit.png" onclick="location.href=\'/profile/ads/' + data + '/edit\'">' +
                    ' <img title="Приостановить" src="/img/grid/pause.png" class="ad-pause" data-id="' + data + '">' +
                    ' <img title="Пометить как проданное" src="/img/grid/close.png" class="ad-close" data-id="' + data + '">' +
                    ' <img title="Удалить" src="/img/grid/delete.png" class="ad-remove" data-id="' + data + '">'
                        ;
                }
            }
        ],
        bFilter: false,
        bInfo: false,
        bLengthChange: false,
        bPaginate: false
    });

    $('table.add-ad .photos .photo .fileupload').fileupload({
        dataType: 'json',
        url: '/ajax/addAdPhoto',
        add: function (e, data) {
            data.submit();
        },
        done: function (e, data) {
            if (data.result.status == 'success' && data.result.data.url) {
                $(this).parent().removeClass('empty')
                    .children('input.filename').val(data.result.data.url)
                .parent()
                    .children('a').attr('href', data.result.data.url).attr('rel', 'photo').show()
                    .children('img').attr('src', data.result.data.url);
            }
        }
    });

});

$(document).on('click', '#add-photo', function(e){
    e.preventDefault();
    $('table.add-ad .photos .photo.empty input').each(function(index){
        if (index === 0) {
            $(this).trigger('click');
        }
    });
});

$(function () {
    var menu_ul = $('.menu > li > ul'),
        menu_a  = $('.menu > li > a');
    menu_ul.hide();
    menu_a.click(function (e) {
        e.preventDefault();
        if (!$(this).hasClass('active')) {
            menu_a.removeClass('active');
            menu_ul.filter(':visible').slideUp('normal');
            $(this).addClass('active').next().stop(true, true).slideDown('normal');
        } else {
            $(this).removeClass('active');
            $(this).next().stop(true, true).slideUp('normal');
        }
    });

    $("#slider").responsiveSlides({
        auto: true,
        speed: 500,
        namespace: "callbacks",
        pager: true
    });
});

addEventListener("load", function(){
    setTimeout(hideURLbar, 0);
}, false);

function hideURLbar(){
    window.scrollTo(0,1);
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});